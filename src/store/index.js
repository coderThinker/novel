import vuex from 'vuex'
import vue from 'vue'
vue.use(vuex)
const store = new vuex.Store({
  // 数据状态管理
  state: {
    token: '', //身份令牌
    userInfo: '', //用户信息
    theme: 'yellow',//阅读页面的背景主题
    mode: 'scroll',//翻页模式
    fontSize: 18,//阅读页面的字体大小
    lineHeight: 20,//15/20/25 三种
  },
  // 响应式修改状态
  mutations: {
    //设置token
    setToken(state, token) {
      state.token = token //token写入state中
      localStorage.setItem('token', token) //token写入storage中
    },
    //删除token
    delToken(state) {
      state.token = ""; //删除state中token
      localStorage.removeItem("token"); //删除storage中token
    },
    //设置用户信息
    setUserInfo(state, userInfo) {
      state.userInfo = userInfo //userInfo写入state中
      localStorage.setItem('userInfo', JSON.stringify(userInfo)) //userInfo写入storage中
    },
    //删除用户信息
    delUserInfo(state) {
      state.userInfo = ''; //删除state中userInfo
      localStorage.removeItem("userInfo"); //删除storage中userInfo
    },
    //修改阅读背景主题
    changeTheme(state, theme) {
      state.theme = theme //修改
      document.documentElement.style.setProperty("--theme", 'var(--' + theme + ')');
      if (theme == 'dark') {
        document.documentElement.style.setProperty("--read-font-color", 'var(--dark-font-color)');
        document.documentElement.style.setProperty("--thin-line-color", 'var(--dark-line)');
      } else {
        document.documentElement.style.setProperty("--read-font-color", 'var(--light-font-color)');
        document.documentElement.style.setProperty("--thin-line-color", 'var(--light-line)');
      }
    },
    //修改翻页模式
    changeMode(state, mode) {
      state.mode = mode //修改
    },
    //修改字体大小
    changeFontSize(state, fontSize) {
      state.fontSize = fontSize //修改
    },
    //修改行高
    changeLineHeight(state, lineHeight) {
      state.lineHeight = lineHeight //修改
    }
  },
  // 全局计算属性
  getters: {
    //获取token
    getToken(state) {
      return state.token || localStorage.getItem('token') || ''
    },
  },
})

//导出
export default store
