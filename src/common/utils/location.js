//获取当前经纬度
function getLocation() {
  return new Promise((resolve, reject) => {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
        location => {
          resolve(location.coords.latitude + ',' + location.coords.longitude) //前纬度后经度
        },
        err => {
          reject(err.message);
        }
      )
    } else {
      reject('浏览器不支持定位')
    }
  })
}

//导出
export default {
  getLocation
}
