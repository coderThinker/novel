// 引入uniapp的uni-webview-js
import * as uni_webview from './uni.webview.js'
document.addEventListener("UniAppJSBridgeReady", function () {
  console.log('webview加载完成')
});

//改变主题样式,通知uniapp修改状态栏字体色和背景色
function themeChange(theme) {
  uni_webview.postMessage({
    data: {
      action: 'themeChange',
      theme
    }
  })
}

//导出函数
export default {
  themeChange
}
