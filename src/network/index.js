import axios from 'axios'
import store from '@/store'
import crypto from './crypto'
//创建axios实例
const instance = axios.create({
  baseURL: 'http://lnglog.cn:9999',
  // baseURL: 'http://127.0.0.1:9999',
})
// 请求拦截器
instance.interceptors.request.use(
  config => {
    // 设置请求头token
    config.headers.Authorization = store.getters.getToken;
    return config
  },
  error => {
    console.log("在request拦截器显示错误：", error.response)
    throw error;
  }
)
//响应拦截器
instance.interceptors.response.use(response => {
  if (response.data.code == 200) {
    return response.data.data //请求成功提取data数据
  } else {
    throw response.data.msg //请求异常抛出异常信息
  }
}, error => {
  throw error.response.data; //抛出错误data数据
})
//发起网络数据请求
function requestdata(data) {
  let postdata = data
  postdata.project = 'novel'
  //加密md5处理
  let md5 = crypto.md5(JSON.stringify(postdata)) //原始MD5
  let md5_produce = '' //锅烧空气
  for (let i = 0; i < md5.length; i++) {
    let asc = md5.slice(i, i + 1).charCodeAt() + 1 //加盐
    let str = String.fromCharCode(asc) //加生抽
    md5_produce += str //加水
  }
  md5_produce = crypto.md5(md5_produce) //出锅
  postdata.sign = md5_produce

  return instance({
    url: '/',
    method: 'post',
    data: postdata,
  })
}
//发起上传文件请求
function upload(data, callback) {
  let postdata = data
  postdata.project = 'novel'

  //加密md5处理
  let md5 = crypto.md5(JSON.stringify(postdata)) //原始MD5
  let md5_produce = '' //锅烧空气
  for (let i = 0; i < md5.length; i++) {
    let asc = md5.slice(i, i + 1).charCodeAt() + 1 //加盐
    let str = String.fromCharCode(asc) //加生抽
    md5_produce += str //加水
  }
  md5_produce = crypto.md5(md5_produce) //出锅
  postdata.sign = md5_produce

  return instance({
    url: '/',
    method: 'post',
    data: postdata,
    onUploadProgress(e) {
      console.log('上传进度：' + Math.round(e.loaded / e.total * 100) + '%')
      callback(Math.round(e.loaded / e.total * 100))
    }
  })
}

//修改头像
function changeAvatar(args) {
  const postdata = {
    api: args[0], //api名称
    size: args[1], //图片大小
    type: args[2], //图片类型
    avatar: args[3], //图片数据
  }
  return upload(postdata, args[4]) //args[4]为回调函数
}
//获取验证码
function getCode(args) {
  const postdata = {
    api: args[0], //api名称
    phone: args[1], //手机号码
  }
  return requestdata(postdata)
}
//用户验证码登录
function codeLogin(args) {
  const postdata = {
    api: args[0], //api名称
    phone: args[1], //手机号码
    code: args[2], //验证码
    location: args[3] //经纬度定位
  }
  return requestdata(postdata)
}
//用户密码登录
function passwordLogin(args) {
  const postdata = {
    api: args[0], //api名称
    username: args[1], //用户名
    password: args[2], //密码
    location: args[3] //经纬度定位
  }
  return requestdata(postdata)
}
//用户设置密码
function setPassword(args) {
  const postdata = {
    api: args[0], //api名称
    password: args[1], //密码
    oldPassword: args[2] //老密码
  }
  return requestdata(postdata)
}
//短信验证码修改密码
function ChangePasswordByCode(args) {
  const postdata = {
    api: args[0], //api名称
    phone: args[1], //手机号码
    code: args[2], //验证码
    location: args[3] //经纬度定位
  }
  return requestdata(postdata)
}
//修改用户基本信息
function changeUserInfo(args) {
  const postdata = {
    api: args[0], //api名称
    nickname: args[1], //昵称
    gender: args[2], //性别
    birthday: args[3], //生日
    introduce: args[4], //签名介绍
  }
  return requestdata(postdata)
}
//提交抽奖结果
function lotteryResult(args) {
  const postdata = {
    api: args[0], //api名称
    gold: args[1], //抽到的金币数量
  }
  return requestdata(postdata)
}
//填写邀请码绑定邀请人
function bindInviteCode(args) {
  const postdata = {
    api: args[0], //api名称
    inviteCode: args[1], //邀请码
  }
  return requestdata(postdata)
}
//搜索小说
function searchNovel(args) {
  const postdata = {
    api: args[0], //api名称
    keyWords: args[1], //搜索关键词(可能是小说名字/作者名字)
  }
  return requestdata(postdata)
}
//请求联想词汇列表
function getImagineWords(args) {
  const postdata = {
    api: args[0], //api名称
    keyWords: args[1], //当前输入的内容
  }
  return requestdata(postdata)
}
//请求小说的详细信息
function getNovelInfo(args) {
  const postdata = {
    api: args[0], //api名称
    table: args[1], //小说表名
  }
  return requestdata(postdata)
}
//提交本次搜索内容
function searchContext(args) {
  const postdata = {
    api: args[0], //api名称
    context: args[1], //搜索内容
  }
  return requestdata(postdata)
}
//提交本次小说点击
function clickNovel(args) {
  const postdata = {
    api: args[0], //api名称
    table: args[1], //小说表名
  }
  return requestdata(postdata)
}
//获取小说的的章节内容
function getNovelChapter(args) {
  const postdata = {
    api: args[0], //api名称
    table: args[1], //小说表名
    index: args[2], //章节索引
  }
  return requestdata(postdata)
}
//获取小说目录全部章节
function getNovelCatalog(args) {
  const postdata = {
    api: args[0], //api名称
    table: args[1], //小说表名
  }
  return requestdata(postdata)
}
//提交阅读时间记录和阅读奖励
function submitReadTime(args) {
  const postdata = {
    api: args[0], //api名称
    minutes: args[1], //阅读分钟数
  }
  return requestdata(postdata)
}
//无post数据函数
function postNull(args) {
  //获取用户信息(getUserInfo)
  //验证token是否有用(preCheckToken)
  const postdata = {
    api: args[1] //api名称
  }
  return requestdata(postdata)
}
//入口函数
function main(...args) {
  return eval(args[0] + '(args)')
}
//导出函数
export default {
  main,
}
