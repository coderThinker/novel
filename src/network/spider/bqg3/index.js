const cheerio = require('cheerio')
import { instance } from '../index'
//获取小说详情信息
function getNovelInfo(args) {
  //args[2]为小说的首页url
  return instance.get(args[2]).then(data => {
    const $ = cheerio.load(data)
    const obj = {} //小说详细信息对象
    obj.name = $('#info > h1').text()//小说名字
    obj.author = $('#info > p:nth-child(4)').text().split('者：')[1]//小说作者
    obj.tags = [$('#info > p:nth-child(2)').text().split('分类：')[1]]//小说类型
    obj.img = 'http://www.ibiquge.com' + $('#fmimg > img')[0].attribs.src //小说封面
    obj.introduce = $('#intro > p:nth-child(1)').text() //小说简介
    obj.status = $('#info > p:nth-child(3)').text().split('态：')[1] //小说状态
    obj.updatetime = new Date($('#info > p:nth-child(7)').text().split('最后更新：')[1]).getTime() //小说最后更新时间戳
    obj.url = args[2] //小说首页地址
    obj.origin = args[1] //小说来源网站
    obj.evaluate = '暂无书评' //小说热门书评
    obj.rate = parseFloat((Math.random() * 5 + 5).toFixed(1)) //小说评分
    obj.wordnumber = Math.round(Math.random() * 10000000) //小说总字数
    obj.clicktimes = Math.round(Math.random() * 10000000) //小说点击次数
    obj.readpeople = Math.round(Math.random() * 10000000) //小说阅读人数
    return obj
  })
}
//获取小说章节目录
function getNovelCatalog(args) {
  //args[2]为小说的首页url
  return instance.get(args[2]).then(data => {
    const $ = cheerio.load(data)
    const catalog = [] //小说目录数组
    for (let item of $('body > div.listmain > dl').children()) {
      const obj = {}
      if (cheerio.load(item)('a')[0]) {
        obj.title = cheerio.load(item)('a').text()//章节标题
        obj.url = '/bqg3' + cheerio.load(item)('a')[0].attribs.href//章节url
        catalog.push(JSON.parse(JSON.stringify(obj)))
      }
    }
    catalog.splice(0, 0, { title: '书封页' })
    return catalog
  })
}
//获取小说指定章节内容
function getNovelChapter(args) {
  //args[2]为章节的首页url
  return instance.get(args[2]).then(data => {
    const $ = cheerio.load(data)
    const chapterContext = {} //小说当前章节的内容
    chapterContext.title = $('#book > div.content > h1').text()//章节标题
    chapterContext.context = $('#content').html().replace(/\s|[\r\n]|&nbsp;/g, '').split('<br><br>')//章节内容
    for (let i in chapterContext.context) {
      chapterContext.context[i] = '\u2003\u2003' + chapterContext.context[i]
    }
    return chapterContext
  })
}

//入口函数
function main(args) {
  //0函数名，1爬虫项目名，后面其他参数
  return eval(args[0] + '(args)')
}

//导出函数
export default main
