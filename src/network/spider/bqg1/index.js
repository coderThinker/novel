const cheerio = require('cheerio')
import { instance } from '../index'

//获取小说详情信息
function getNovelInfo(args) {
  instance.get(args[2]).then(data => {
    console.log(data)
  })
}

//获取小说章节目录
function getNovelCatalog(args) {
  console.log('章节目录', args)
}


//获取小说指定章节内容
function getNovelChapter(args) {
  console.log('章节内容', args)
}



//入口函数
function main(args) {
  //0函数名，1爬虫项目名，后面其他参数
  return eval(args[0] + '(args)')
}

//导出函数
export default main
