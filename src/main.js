import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import location from './common/utils/location.js'
import postMessage from './common/utils/message.js'
import network from './network/index.js'
Vue.prototype.$postMessage = postMessage //注册webview通信封装类
Vue.prototype.$location = location //注册定位工具
Vue.prototype.$network = network //注册网络请求事件
Vue.config.productionTip = false //阻止显示生产模式的消息

//注册全局组件
import popup from "@/components/k-popup/popup.vue";
Vue.component('k-popup', popup)
import button from "@/components/k-button/button.vue";
Vue.component('k-button', button)
import toptab from "@/components/k-toptab/toptab.vue";
Vue.component('k-toptab', toptab)
import scroll from "@/components/k-scroll/scroll.vue";
Vue.component('k-scroll', scroll)
//更新用户信息
function updateUserInfo() {
  return network.main('postNull', 'getUserInfo').then(data => {
    store.commit('setUserInfo', data) //把用户信息储存到store中
  }).catch(err => {
    this.$store.commit('delUserInfo') //清除用户信息
    console.log(err)
  })
}
Vue.prototype.$updateUserInfo = updateUserInfo //注册更新用户信息全局函数

// 引入vant
import {
  Loading,
  Lazyload,
  ImagePreview,
  NumberKeyboard,
  Toast,
  PasswordInput,
  Dialog,
  ShareSheet,
  Picker,
  Popup,
  Rate,
  Slider,
  Switch
} from 'vant';
import 'vant/lib/loading/style';
import 'vant/lib/lazyload/style';
import 'vant/lib/image-preview/style';
import 'vant/lib/number-keyboard/style';
import 'vant/lib/toast/style';
import 'vant/lib/password-input/style';
import 'vant/lib/dialog/style';
import 'vant/lib/share-sheet/style';
import 'vant/lib/picker/style';
import 'vant/lib/popup/style';
import 'vant/lib/rate/style';
import 'vant/lib/slider/style';
import 'vant/lib/switch/style';
// 引入全部样式
import 'vant/lib/index.less';
Vue.use(Loading);
Vue.use(ImagePreview);
Vue.use(NumberKeyboard);
Vue.use(Toast);
Vue.use(PasswordInput);
Vue.use(Dialog);
Vue.use(ShareSheet);
Vue.use(Picker);
Vue.use(Popup);
Vue.use(Rate);
Vue.use(Slider);
Vue.use(Switch);
Vue.use(Lazyload, { loading: require('@/assets/img/other/loading.png'), error: require('@/assets/img/other/failed.png') });
new Vue({
  router,
  store,
  render: function (h) {
    return h(App)
  }
}).$mount('#app')
