import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '@/store'
import network from '@/network'

Vue.use(VueRouter)

const noSupport = () => import('@/views/noSupport.vue') //不支持页面
const main = () => import('@/views/main/index.vue') //主要页面

const read = () => import('@/views/read/index.vue') //阅读页面
const Search = () => import('@/views/Search/index.vue') //搜索页面
const searchResult = () => import('@/views/searchResult/index.vue') //搜索结果页面
const setting = () => import('@/views/setting/index.vue') //设置
const bookrack = () => import('@/views/bookrack/index.vue') //书架
const customer = () => import('@/views/customer/index.vue') //联系客服
const profile = () => import('@/views/profile/index.vue') //个人主页
const invite = () => import('@/views/invite/index.vue') //邀请好友
const goldAccount = () => import('@/views/goldAccount/index.vue') //金币明细
const inviteCode = () => import('@/views/inviteCode/index.vue') //输入邀请码
const agreement = () => import('@/views/agreement/index.vue') //用户协议
const privacy = () => import('@/views/privacy/index.vue') //隐私政策
const moreSet = () => import('@/views/moreSet/index.vue') //阅读页面的更多设置

//全部关于登录的页面
const login = () => import('@/views/login/index.vue')
const loginByPassword = () => import('@/views/login/loginByPassword/index.vue')
const inputCode = () => import('@/views/login/inputCode/index.vue')
const inputCode2 = () => import('@/views/login/inputCode2/index.vue')
const setPassword = () => import('@/views/login/setPassword/index.vue')
const setPassword2 = () => import('@/views/login/setPassword2/index.vue')
const changePassword = () => import('@/views/login/changePassword/index.vue')
const foundPassword = () => import('@/views/login/foundPassword/index.vue')
const foundPassword2 = () => import('@/views/login/foundPassword2/index.vue')

const router = new VueRouter({
  mode: 'history',
  routes: [{
    path: '/noSupport',
    component: noSupport,
    meta: {
      title: '不支持页面',
      index: 0
    }
  },
  {
    path: '/main',
    component: main,
    meta: {
      title: '首页',
      index: 0
    }
  },
  {
    path: '/Search',
    component: Search,
    meta: {
      title: '搜索',
      index: 1
    }
  },
  {
    path: '/searchResult',
    component: searchResult,
    meta: {
      title: '搜索结果',
      index: 2
    }
  },
  {
    path: '/read',
    component: read,
    meta: {
      title: '阅读',
      index: 4
    }
  },
  {
    path: '/login',
    component: login,
    meta: {
      title: '登录',
      index: 1
    }
  },
  {
    path: '/loginByPassword',
    component: loginByPassword,
    meta: {
      title: '密码登录',
      index: 2
    }
  },
  {
    path: '/inputCode',
    component: inputCode,
    meta: {
      title: '输入验证码',
      index: 2
    }
  },
  {
    path: '/foundPassword2',
    component: foundPassword2,
    meta: {
      title: '找回密码',
      index: 3
    }
  },
  {
    path: '/inputCode2',
    component: inputCode2,
    meta: {
      title: '输入验证码',
      index: 4
    }
  },
  {
    path: '/setPassword2',
    component: setPassword2,
    meta: {
      title: '设置密码',
      index: 5
    }
  },
  {
    path: '/setPassword',
    component: setPassword,
    meta: {
      title: '设置密码',
      index: 2
    }
  },
  {
    path: '/changePassword',
    component: changePassword,
    meta: {
      title: '修改密码',
      index: 2
    }
  },
  {
    path: '/foundPassword',
    component: foundPassword,
    meta: {
      title: '找回密码',
      index: 3
    }
  },
  {
    path: '/setting',
    component: setting,
    meta: {
      title: '设置',
      index: 1
    }
  },
  {
    path: '/bookrack',
    component: bookrack,
    meta: {
      title: '书架',
      index: 1,
      requireAuth: true
    }
  },
  {
    path: '/invite',
    component: invite,
    meta: {
      title: '邀请好友',
      index: 1,
      requireAuth: true
    }
  },
  {
    path: '/customer',
    component: customer,
    meta: {
      title: '联系客服',
      index: 1
    }
  },
  {
    path: '/profile',
    component: profile,
    meta: {
      title: '个人主页',
      index: 1,
      requireAuth: true
    }
  },
  {
    path: '/goldAccount',
    component: goldAccount,
    meta: {
      title: '金币明细',
      index: 2,
      requireAuth: true
    }
  },
  {
    path: '/inviteCode',
    component: inviteCode,
    meta: {
      title: '填写邀请码',
      index: 2,
      requireAuth: true
    }
  },
  {
    path: '/agreement',
    component: agreement,
    meta: {
      title: '用户协议',
      index: 2,
    }
  }
  ]
})


//全局导航守卫
router.beforeEach((to, from, next) => {
  //更改标题
  document.title = to.meta.title
  //拦截
  if (to.meta.requireAuth) {
    //有token
    if (store.getters.getToken) {
      network.main('postNull', 'preCheckToken').then(data => {
        next(); //验证token是否可用
      }).catch(err => { //请求失败
        router.push("/login")
      })
    } else {
      router.push("/login")
    }
  } else {
    next(); //如果无需token,那么随它去吧
  }

})

export default router
