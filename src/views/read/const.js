//目录的popup参数配置
const catalogue = {
  //对话框的宽度，单位相对于浏览器的百分比
  popWidth: 85,
  //对话框的高度，单位相对于浏览器的百分比
  popHeight: 100,
  //对话框隐藏时的X位置，单位相对于浏览器的百分比
  popHiddenX: -85,
  //对话框隐藏时的Y位置，单位相对于浏览器的百分比
  popHiddenY: 0,
  //对话框出现时的X位置，单位相对于浏览器的百分比
  popShowX: 0,
  //对话框出现时的Y位置，单位相对于浏览器的百分比
  popShowY: 0,
  //过渡旋转角度
  rotate: 0,
  //使用缩放过渡
  useScale: false,
  //使用透明度变化
  useOpacity: false,
  //开启外部点击关闭弹窗
  outside: true,
}


//导出
export {
  catalogue,
}
