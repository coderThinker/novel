const path = require('path')
module.exports = {
  //关闭代码检测
  lintOnSave: false,
  devServer: {
    port: 8888,
    open: true,
    host: '127.0.0.1',
  },
  css: {
    loaderOptions: {
      less: {
        // 若 less-loader 版本小于 6.0，请移除 lessOptions 这一级，直接配置选项。
        lessOptions: {
          modifyVars: {
            // 直接覆盖变量
            'dialog-message-font-size': '0.16rem',
            'rate-icon-size': '0.13rem',
          },
        },
      },
    },
  },
  productionSourceMap: false,//取消map打包
  pages: {
    index: {
      // page 的入口
      entry: 'src/main.js',
      // 模板来源
      template: 'public/index.html',
      // 在 dist/index.html 的输出
      filename: 'index.html',
    }
  },
  publicPath: '/novel'
}
